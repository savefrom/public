#!/bin/sh
if [ -z $TEMPLATE_DIR ]
then 
    TEMPLATE_DIR="/var/www/app"
fi

for inputfile in $(find $TEMPLATE_DIR -type f -iname "*.tpl"); do
    outputfile=$(echo $inputfile | sed s/\.tpl//g)
    envsubst < $inputfile > $outputfile
    rm -rf $inputfile
done

cat << EOF > /usr/local/etc/php-fpm.d/www.conf
[www]
listen = 127.0.0.1:9000
user = www-data
group = www-data
pm = static
pm.max_children =  $MAX_CHILDREN
pm.start_servers =  $START_SERVERS 
pm.min_spare_servers =  $MIN_SPARE_SERVERS 
pm.max_spare_servers =  $MAX_SPARE_SERVERS 
pm.max_requests = 5000
request_terminate_timeout = 60
slowlog = /var/log/php-fpm/slow.log
php_flag[display_errors] = off
php_admin_flag[log_errors] = on
php_admin_value[upload_tmp_dir] = /var/lib/php/tmp
EOF

php-fpm